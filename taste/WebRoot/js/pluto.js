function logs(o){
	console.log(o);
}
function Ajax(opt){
	var defaultOpt={
	  type: 'GET',
	  url: '/',
	  // data to be added to query string:
	  data: {},
	  // type of data we are expecting in return:
	  dataType: 'json',
	  timeout: 3000,
	  beforeSend:function(xhr, settings){
		  $.showIndicator();
	  },
	  success: function(data){
		  console.log(data);
	  },
	  error: function(xhr, type){
	    alert('Ajax error!');
	  },
	  complete:function(xhr, status){
		  $.hideIndicator();
	  }
	};
	var targetOpt={};
	$.extend(true, targetOpt,defaultOpt,opt);
	console.log(targetOpt);
	$.ajax(targetOpt);
	
}
function Get(url,callback,dataType){
	var opt={};
	opt.url=url;
	opt.success=callback;
	if(dataType)
		opt.dataType=dataType;
	Ajax(opt);
}
function Post(url,data,callback,dataType){
	var opt={};
	opt.url=url;
	opt.type="POST";
	opt.data=data;
	opt.success=callback;
	if(dataType)
		opt.dataType=dataType;
	Ajax(opt);
}
function getRequest() {   
    var url = location.search;
    var theRequest = new Object();   
    if (url.indexOf("?") != -1) {   
       var str = url.substr(url.indexOf("?")+1); 
       strs = str.split("&");   
       for(var i = 0; i < strs.length; i ++) {   
          theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);   
       }   
    }   
    return theRequest;   
}   