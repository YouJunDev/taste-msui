package pluto.controller;

import pluto.controller.base.BaseController;
import pluto.controller.dto.MyAppJSON;
import pluto.model.Restaurant;

public class RestaurantController extends BaseController {
	public void index(){
		myRender("/restaurant.html");;
	}
	
	public void show(){
//		setAttr("one", Restaurant.dao.findById(getParaToInt(0)));
//		myRender("/restaurant/detail.html");
		MyAppJSON<Restaurant> appJSON=new MyAppJSON<Restaurant>();
		appJSON.addItem(Restaurant.dao.findById(getParaToInt(0)));
		renderJson(appJSON);
	}
}
