package pluto.controller.index;

import java.util.List;

import pluto.controller.base.BaseController;
import pluto.controller.dto.MyAppJSON;
import pluto.model.Master;
import pluto.model.News;
import pluto.model.Restaurant;
import pluto.model.Show;

import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Model;

public class IndexController extends BaseController {
	public void index(){
		System.out.println(getAttr("BASE_URL"));
		render("/index.html");
	}
	
	
	public void initJSON(){
		MyAppJSON<Model> appJSON=new MyAppJSON();
		appJSON.addItemMap("showList", Show.dao.findForIndex(3).getList());
		appJSON.addItemMap("newsList", News.dao.findForIndex(3).getList());
		appJSON.addItemMap("masterList",Master.dao.findForIndex(2).getList());
		appJSON.addItemMap("restaurantList",Restaurant.dao.findForIndex(3).getList());
		renderJson(appJSON);
	}
}
