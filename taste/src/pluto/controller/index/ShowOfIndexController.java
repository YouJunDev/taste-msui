package pluto.controller.index;

import pluto.controller.base.BaseController;
import pluto.controller.dto.MyAppJSON;
import pluto.model.News;
import pluto.model.Restaurant;
import pluto.model.Show;

import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.Restful;
public class ShowOfIndexController extends BaseController {
	public void index(){
	}

	public void initJSON(){
		MyAppJSON<Show> appJSON=new MyAppJSON();
		appJSON.addItems(Show.dao.findForIndex(10).getList());
		renderJson(appJSON);
	}
}
